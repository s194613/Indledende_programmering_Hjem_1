import java.util.*;

public class Cpr {

	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		String input = console.nextLine();
		
		try {
			String cpr = format(input);
			int last4digits = Integer.parseInt(cpr.substring(6));
			
			if(isEven(last4digits)) System.out.print("lige cprnummer");
			else System.out.print("ulige cprnummer");
		} catch(NumberFormatException e) {
			System.out.print(format(input));
		}
		
		
	}
	
	public static String format(String s) {
		String cpr = s.replaceAll("\\s", "");
		
		if(cpr.matches("[0-9]+") && cpr.length()==10) return cpr;
		return "ikke et cprnummer";
	}
	
	public static boolean isEven(int n) {
		 if(n%2==0) return true;
		 return false;
	}
	


}
