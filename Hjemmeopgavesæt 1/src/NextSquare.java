import java.util.*;

public class NextSquare {
	
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		
		try {
			int input = console.nextInt();
			int nextSquare = nextSquare(input);
			System.out.print(nextSquare);
		} catch(InputMismatchException e) {
			System.out.print("Not a number");
		} catch(NegativeArraySizeException e) {
			System.out.print("Not a positive number");
		}
		
	}
	
	public static int nextSquare(int n) {
		int[] square = new int[n+1];
		
		for(int i=1; i<=n+1; i++) {
			square[i-1] = i*i;
		}
		
		return square[n];
	}

}
